Script to check FF version and downgrade it if needed.
========================================================

Script was prepared in PowerShell.
It can be used from Jenkins using Windows Power Shell  (as build step)

This Jenkins job can be executed in another job (using 'Trigger/call builds on other projects' as build step. It needs to be invoked before maven goal - test)